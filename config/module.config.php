<?php

namespace Lerp\ProductCalc;

use Lerp\Product\Service\Calc\ProductCalcService;
use Lerp\ProductCalc\Controller\Rest\ProductCalcController;
use Lerp\ProductCalc\Factory\Controller\Rest\ProductCalcControllerFactory;
use Lerp\ProductCalc\Factory\Form\ProductCalcFormFactory;
use Lerp\ProductCalc\Factory\Service\ProductCalcServiceFactory;
use Lerp\ProductCalc\Factory\Table\ProductCalcTableFactory;
use Lerp\ProductCalc\Form\ProductCalcForm;
use Lerp\ProductCalc\Table\ProductCalcTable;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'lerp_productcalc_rest_product_productcalc' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/lerp-product-calc[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => ProductCalcController::class,
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            ProductCalcController::class => ProductCalcControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            ProductCalcService::class => ProductCalcServiceFactory::class, // overwrite from Lerp\Product
            // table
            ProductCalcTable::class => ProductCalcTableFactory::class,
            // form
            ProductCalcForm::class => ProductCalcFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
