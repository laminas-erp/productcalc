# Laminas Module lerp/product-calc

[Lingana ERP](https://linganaerp.de)

Overwrites the base service:
```php
\Lerp\Product\Service\Calc\ProductCalcService::class
```

Only one product_calc per product!
