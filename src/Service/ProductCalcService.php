<?php

namespace Lerp\ProductCalc\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Service\Calc\ProductCalcServiceInterface;
use Lerp\Product\Service\ProductWorkflowService;
use Lerp\Product\Table\ProductListTable;
use Lerp\Product\Table\ProductTable;
use Lerp\ProductCalc\Entity\ProductCalcEntity;
use Lerp\ProductCalc\Table\ProductCalcTable;

class ProductCalcService extends AbstractService implements ProductCalcServiceInterface
{
    protected array $workflowTypes;
    protected ProductCalcTable $productCalcTable;
    protected ProductTable $productTable;
    protected ProductListTable $productListTable;
    protected ProductWorkflowService $productWorkflowService;

    public function setWorkflowTypes(array $workflowTypes): void
    {
        $this->workflowTypes = $workflowTypes;
    }

    public function setProductCalcTable(ProductCalcTable $productCalcTable): void
    {
        $this->productCalcTable = $productCalcTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    public function setProductListTable(ProductListTable $productListTable): void
    {
        $this->productListTable = $productListTable;
    }

    public function setProductWorkflowService(ProductWorkflowService $productWorkflowService): void
    {
        $this->productWorkflowService = $productWorkflowService;
    }

    /**
     * @param string $productUuid
     * @param string $userUuid
     * @return array
     */
    public function createGetProductCalc(string $productUuid, string $userUuid): array
    {
        if(empty($product = $this->productTable->getProduct($productUuid))) {
            return [];
        }
        return $this->productCalcTable->createGetProductCalc($productUuid, $product['quantityunit_uuid'], $userUuid);
    }

    /**
     * Create explicit a new productCalc.
     * @param ProductCalcEntity $productCalcEntity
     * @param string $userUuid
     * @return string `product_calc_uuid` or an empty string.
     */
    public function createProductCalcWithEntity(ProductCalcEntity $productCalcEntity, string $userUuid): string
    {
        return $this->productCalcTable->createProductCalcWithEntity($productCalcEntity, $userUuid);
    }

    /**
     * @param string $productCalcUuid
     * @return ProductCalcEntity|null
     */
    public function calculateProductCalc(string $productCalcUuid): ProductCalcEntity|null
    {
        $productCalc = $this->productCalcTable->getProductCalc($productCalcUuid);
        if (empty($productCalc)) {
            $this->message = 'ProductCalc is empty in ' . __CLASS__ . '()->' . __FUNCTION__;
            return null;
        }
        $productCalcEntity = new ProductCalcEntity();
        $productCalcEntity->exchangeArrayFromDatabase($productCalc);
        if (!$productCalcEntity->validCost()) {
            $this->message = 'All product_calc_cost_* are empty.';
            return null;
        }
        return $this->calculateProductCalcEntity($productCalcEntity);
    }

    public function calculateProductCalcEntity(ProductCalcEntity $entity): ProductCalcEntity
    {
        $this->computeCostAll($entity);
        return $entity;
    }

    protected function computeCostAll(ProductCalcEntity $entity): void
    {
        $entity->computeCostBaseRes();
        $this->computeCostWorkRes($entity);
        $this->computeCostWorkExtern($entity);

        $entity->setProductCalcManufactPrice(
            $entity->getProductCalcCostBaseRes()
            + $entity->getProductCalcCostWorkRes()
            + $entity->getProductCalcCostWorkExtern()
        );

        $vvgkBase = $entity->getProductCalcCostBaseRes() + ($entity->getProductCalcCostBaseRes() / 100) * $entity->getProductCalcPercentVvgk();
        $vvgkWork = $entity->getProductCalcCostWorkRes() + ($entity->getProductCalcCostWorkRes() / 100) * $entity->getProductCalcPercentVvgk();
        $profitExtern = $entity->getProductCalcCostWorkExtern() + ($entity->getProductCalcCostWorkExtern() / 100) * $entity->getProductCalcPercentProfitExtern();
        $profit = $vvgkBase + $vvgkWork + (($vvgkBase + $vvgkWork) / 100) * $entity->getProductCalcPercentProfit();
        $entity->setProductCalcPrice($profit + $profitExtern);
    }

    protected function computeCostWorkRes(ProductCalcEntity $productCalcEntity): void
    {
        $workflows = $this->productWorkflowService->getProductWorkflows($productCalcEntity->getProductUuid(), '', false);
        $costWork = 0;
        foreach ($workflows as $workflow) {
            $costWork += ($workflow['product_workflow_time'] / 60) * $workflow['product_workflow_price_per_hour'];
        }
        $productCalcEntity->setProductCalcCostWork($costWork);
        // Arbeit intern plus Gewinn
        $productCalcEntity->setProductCalcCostWorkRes($costWork + ($costWork / 100) * $productCalcEntity->getProductCalcPercentFgk());
    }

    protected function computeCostWorkExtern(ProductCalcEntity $productCalcEntity): void
    {
        $workflows = $this->productWorkflowService->getProductWorkflows($productCalcEntity->getProductUuid(), '', true);
        $costWork = 0;
        foreach ($workflows as $workflow) {
            $costWork += ($workflow['product_workflow_time'] / 60) * $workflow['product_workflow_price_per_hour'];
        }
        $productCalcEntity->setProductCalcCostWorkExtern($costWork);
    }

    /**
     * @param string $productCalcUuid
     * @return array
     */
    public function getProductCalc(string $productCalcUuid): array
    {
        return $this->productCalcTable->getProductCalc($productCalcUuid);
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductCalcLastForProduct(string $productUuid): array
    {
        return $this->productCalcTable->getProductCalcLastForProduct($productUuid);
    }

    public function getProductCalcsForProduct(string $productUuid): array
    {
        return $this->productCalcTable->getProductCalcsForProduct($productUuid);
    }

    /**
     * A product has at least one product_calc.
     * Here you can chef if this is valid (not only initial).
     * @param string $productUuid
     * @return bool
     */
    public function hasProductOnlyProductCalcInitial(string $productUuid): bool
    {
        if (count($productCalcs = $this->getProductCalcsForProduct($productUuid)) > 1) {
            return false;
        }
        $productCalcEntity = new ProductCalcEntity();
        $productCalcEntity->exchangeArrayFromRequest($productCalcs[0]);
        if ($productCalcEntity->validCost()) {
            return false;
        }
        return true;
    }

    /**
     * @param array $productCalc
     * @param string $userUuid
     * @return bool
     */
    public function updateProductCalc(array $productCalc, string $userUuid): bool
    {
        $productCalcEntity = new ProductCalcEntity();
        $productCalcEntity->exchangeArrayFromRequest($productCalc);
        if (!$productCalcEntity->validCost()) {
            $this->message = 'All product_calc_cost_* are empty.';
            return 0;
        }
        return $this->updateProductCalcWithEntity($productCalcEntity, $userUuid) >= 0;
    }

    /**
     * Update (do not create a new one) an existing productCalc.
     * @param ProductCalcEntity $productCalcEntity
     * @param string $userUuid
     * @return bool
     */
    public function updateProductCalcWithEntity(ProductCalcEntity $productCalcEntity, string $userUuid): bool
    {
        return $this->productCalcTable->updateProductCalcEntity($productCalcEntity, $userUuid) >= 0;
    }
}
