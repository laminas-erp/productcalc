<?php

namespace Lerp\ProductCalc\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class ProductCalcEntity extends AbstractEntity
{
    protected array $mapping = [
        'product_calc_uuid'                  => 'product_calc_uuid',
        'product_uuid'                       => 'product_uuid',
        'quantityunit_uuid'                  => 'quantityunit_uuid',
        'product_calc_time_create'           => 'product_calc_time_create',
        'product_calc_time_update'           => 'product_calc_time_update',
        'user_uuid_create'                   => 'user_uuid_create',
        'user_uuid_update'                   => 'user_uuid_update',
        'product_calc_cost_base'             => 'product_calc_cost_base',
        'product_calc_cost_work'             => 'product_calc_cost_work',
        'product_calc_cost_work_extern'      => 'product_calc_cost_work_extern',
        'product_calc_cost_base_res'         => 'product_calc_cost_base_res',
        'product_calc_cost_work_res'         => 'product_calc_cost_work_res',
        'product_calc_manufact_price'        => 'product_calc_manufact_price',
        'product_calc_price'                 => 'product_calc_price',
        'product_calc_percent_mgk'           => 'product_calc_percent_mgk',
        'product_calc_percent_fgk'           => 'product_calc_percent_fgk',
        'product_calc_percent_vvgk'          => 'product_calc_percent_vvgk',
        'product_calc_percent_profit'        => 'product_calc_percent_profit',
        'product_calc_percent_profit_extern' => 'product_calc_percent_profit_extern',

        'product_calc_sum_cost_base' => 'product_calc_sum_cost_base',
        'product_calc_sum_cost_work' => 'product_calc_sum_cost_work',

        'product_calc_sum_cost_work_extern' => 'product_calc_sum_cost_work_extern', // Arbeit Stuecklisten
        'product_calc_sum_cost_base_res'    => 'product_calc_sum_cost_base_res',
        'product_calc_sum_cost_work_res'    => 'product_calc_sum_cost_work_res',

        'product_calc_sum_manufact_price' => 'product_calc_sum_manufact_price',
        'product_calc_sum_price'          => 'product_calc_sum_price',

        'product_calc_time' => 'product_calc_time',

        'product_calc_price_set' => 'product_calc_price_set',
    ];

    protected $primaryKey = 'product_calc_uuid';

    /**
     * @var array All products from product_list.
     */
    protected array $products;

    /**
     * @var bool
     */
    protected bool $computeCostAllCalled = false;

    public function unsetReadonly(): void
    {
        if (isset($storage['product_calc_time_create'])) {
            unset($this->storage['product_calc_time_create']);
        }
        if (isset($storage['product_calc_time_update'])) {
            unset($this->storage['product_calc_time_update']);
        }
        if (isset($storage['user_uuid_create'])) {
            unset($this->storage['user_uuid_create']);
        }
        if (isset($storage['user_uuid_update'])) {
            unset($this->storage['user_uuid_update']);
        }
        if (isset($storage['product_calc_time_create'])) {
            unset($this->storage['product_calc_time_create']);
        }
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function getProductCalcCostBase(): float
    {
        if (!isset($this->storage['product_calc_cost_base'])) {
            return 0;
        }
        return $this->storage['product_calc_cost_base'];
    }

    public function setProductCalcCostBase(float $productCalcCostBase): void
    {
        $this->storage['product_calc_cost_base'] = $productCalcCostBase;
    }

    public function getProductCalcCostWork(): float
    {
        if (!isset($this->storage['product_calc_cost_work'])) {
            return 0;
        }
        return $this->storage['product_calc_cost_work'];
    }

    public function setProductCalcCostWork(float $productCalcCostWork): void
    {
        $this->storage['product_calc_cost_work'] = $productCalcCostWork;
    }

    public function getProductCalcCostWorkExtern(): float
    {
        if (!isset($this->storage['product_calc_cost_work_extern'])) {
            return 0;
        }
        return $this->storage['product_calc_cost_work_extern'];
    }

    public function setProductCalcCostWorkExtern(float $productCalcCostWorkExtern): void
    {
        $this->storage['product_calc_cost_work_extern'] = $productCalcCostWorkExtern;
    }

    public function getProductCalcCostBaseRes(): float
    {
        if (!isset($this->storage['product_calc_cost_base_res'])) {
            return 0;
        }
        return $this->storage['product_calc_cost_base_res'];
    }

    public function setProductCalcCostBaseRes(float $productCalcCostBaseRes): void
    {
        $this->storage['product_calc_cost_base_res'] = $productCalcCostBaseRes;
    }

    public function getProductCalcCostWorkRes(): float
    {
        if (!isset($this->storage['product_calc_cost_work_res'])) {
            return 0;
        }
        return $this->storage['product_calc_cost_work_res'];
    }

    public function setProductCalcCostWorkRes(float $productCalcCostWorkRes): void
    {
        $this->storage['product_calc_cost_work_res'] = $productCalcCostWorkRes;
    }

    public function getProductCalcManufactPrice(): float
    {
        if (!isset($this->storage['product_calc_manufact_price'])) {
            return 0;
        }
        return $this->storage['product_calc_manufact_price'];
    }

    public function setProductCalcManufactPrice(float $productCalcManufactPrice): void
    {
        $this->storage['product_calc_manufact_price'] = $productCalcManufactPrice;
    }

    public function getProductCalcPrice(): float
    {
        if (!isset($this->storage['product_calc_price'])) {
            return 0;
        }
        return $this->storage['product_calc_price'];
    }

    public function setProductCalcPrice(float $productCalcPrice): void
    {
        $this->storage['product_calc_price'] = $productCalcPrice;
    }

    /**
     * Material Gemein Kosten
     * @return float
     */
    public function getProductCalcPercentMgk(): float
    {
        if (!isset($this->storage['product_calc_percent_mgk'])) {
            return 0;
        }
        return $this->storage['product_calc_percent_mgk'];
    }

    /**
     * Fertigung Gemein Kosten
     * @return float
     */
    public function getProductCalcPercentFgk(): float
    {
        if (!isset($this->storage['product_calc_percent_fgk'])) {
            return 0;
        }
        return $this->storage['product_calc_percent_fgk'];
    }

    /**
     * Vertrieb Verwaltung Gemein Kosten
     * @return float
     */
    public function getProductCalcPercentVvgk(): float
    {
        if (!isset($this->storage['product_calc_percent_vvgk'])) {
            return 0;
        }
        return $this->storage['product_calc_percent_vvgk'];
    }

    /**
     * Profit
     * @return float
     */
    public function getProductCalcPercentProfit(): float
    {
        if (!isset($this->storage['product_calc_percent_profit'])) {
            return 0;
        }
        return $this->storage['product_calc_percent_profit'];
    }

    /**
     * Profit Extern
     * @return float
     */
    public function getProductCalcPercentProfitExtern(): float
    {
        if (!isset($this->storage['product_calc_percent_profit_extern'])) {
            return 0;
        }
        return $this->storage['product_calc_percent_profit_extern'];
    }

    public function getProductCalcSumCostBase(): float
    {
        if (!isset($this->storage['product_calc_sum_cost_base'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_cost_base'];
    }

    public function getProductCalcSumCostWork(): float
    {
        if (!isset($this->storage['product_calc_sum_cost_work'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_cost_work'];
    }

    public function getProductCalcSumCostWorkExtern(): float
    {
        if (!isset($this->storage['product_calc_sum_cost_work_extern'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_cost_work_extern'];
    }

    public function getProductCalcSumCostBaseRes(): float
    {
        if (!isset($this->storage['product_calc_sum_cost_base_res'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_cost_base_res'];
    }

    public function getProductCalcSumCostWorkRes(): float
    {
        if (!isset($this->storage['product_calc_sum_cost_work_res'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_cost_work_res'];
    }

    public function getProductCalcSumManufactPrice(): float
    {
        if (!isset($this->storage['product_calc_sum_manufact_price'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_manufact_price'];
    }

    public function getProductCalcSumPrice(): float
    {
        if (!isset($this->storage['product_calc_sum_price'])) {
            return 0;
        }
        return $this->storage['product_calc_sum_price'];
    }

    public function setProductCalcSumPrice(float $productCalcSumPrice): void
    {
        $this->storage['product_calc_sum_price'] = $productCalcSumPrice;
    }

    public function getProductCalcTime(): string
    {
        if (!isset($this->storage['product_calc_time'])) {
            return '';
        }
        return new $this->storage['product_calc_time'];
    }

    public function getProductCalcPriceSet(): float
    {
        if (!isset($this->storage['product_calc_price_set'])) {
            return 0;
        }
        return $this->storage['product_calc_price_set'];
    }

    public function setProductCalcPriceSet(float $productCalcPriceSet): void
    {
        $this->storage['product_calc_price_set'] = $productCalcPriceSet;
    }

    /**
     * Check if at least one product_calc_cost_* must be > 0
     * @return bool
     */
    public function validCost(): bool
    {
        return !empty($this->getProductCalcCostBase()) || !empty($this->getProductCalcCostWork()) || !empty($this->getProductCalcCostWorkExtern());
    }

    /**
     * set addition of productCalcCostBase
     */
    public function computeCostBaseRes(): void
    {
        $this->setProductCalcCostBaseRes(
            $this->getProductCalcCostBase()
            + ($this->getProductCalcCostBase() / 100)
            * $this->getProductCalcPercentMgk()
        );
    }

}
