<?php

namespace Lerp\ProductCalc\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\ProductCalc\Entity\ProductCalcEntity;
use Lerp\ProductCalc\Form\ProductCalcForm;
use Lerp\ProductCalc\Service\ProductCalcService;
use Laminas\Http\Response;

class ProductCalcController extends AbstractUserRestController
{
    protected ProductCalcService $productCalcService;
    protected ProductCalcForm $productCalcForm;

    public function setProductCalcService(ProductCalcService $productCalcService): void
    {
        $this->productCalcService = $productCalcService;
    }

    public function setProductCalcForm(ProductCalcForm $productCalcForm): void
    {
        $this->productCalcForm = $productCalcForm;
    }

    /**
     * Explicit create a productCalc with the data send.
     * @param array $data
     * @return JsonModel With the created product-calculation.
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->productCalcForm->setProductUuidAvailable(true);
        $this->productCalcForm->init();
        $this->productCalcForm->setData($data);
        if (!$this->productCalcForm->isValid()) {
            $jsonModel->addMessages($this->productCalcForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->productCalcForm->getData();
        $productCalcEntity = new ProductCalcEntity();
        $productCalcEntity->exchangeArrayFromDatabase($formData);
        if (!$productCalcEntity->validCost()) {
            $jsonModel->addMessage('All product_calc_cost_* are empty');
            return $jsonModel;
        }
        $this->productCalcService->calculateProductCalcEntity($productCalcEntity);
        if ($this->productCalcService->hasProductOnlyProductCalcInitial($productCalcEntity->getProductUuid())) {
            if ($this->productCalcService->updateProductCalcWithEntity($productCalcEntity, $this->userService->getUserUuid())) {
                $jsonModel->setUuid($productCalcEntity->getUuid());
                $jsonModel->setSuccess(1);
            }
        } else {
            if (!empty($uuid = $this->productCalcService->createProductCalcWithEntity($productCalcEntity, $this->userService->getUserUuid()))) {
                $jsonModel->setUuid($uuid);
                $jsonModel->setSuccess(1);
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            }
        }
        return $jsonModel;
    }

    /**
     * Create one or get the last productCalculation for productUuid.
     * @param string $id product UUID
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->productCalcService->createGetProductCalc($id, $this->userService->getUserUuid()));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * With query param `only_calc` == true it will do no update in the database.
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $onlyCalc = $this->params()->fromQuery('only_calc') == 'true';
        $data['product_calc_uuid'] = $id;
        $this->productCalcForm->setProductUuidAvailable(true);
        $this->productCalcForm->init();
        $this->productCalcForm->setData($data);
        if (!$this->productCalcForm->isValid()) {
            $jsonModel->addMessages($this->productCalcForm->getMessages());
            return $jsonModel;
        }
        $formData = $this->productCalcForm->getData();
        $productCalcEntity = new ProductCalcEntity();
        $productCalcEntity->exchangeArrayFromDatabase($formData);
        $this->productCalcService->calculateProductCalcEntity($productCalcEntity);
        if (!$onlyCalc && !$this->productCalcService->updateProductCalcWithEntity($productCalcEntity, $this->userService->getUserUuid())) {
            $jsonModel->addMessage($this->productCalcService->getMessage());
        } else {
            $jsonModel->setSuccess(1);
        }
        $jsonModel->setObj($productCalcEntity->getStorage());
        return $jsonModel;
    }

    /**
     * Return all product_calc entries for one product (query param `productUuid`).
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($productUuid = filter_input(INPUT_GET, 'productUuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->productCalcService->getProductCalcsForProduct($productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
