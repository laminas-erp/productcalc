<?php

namespace Lerp\ProductCalc\Factory\Service;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Lerp\Product\Service\ProductWorkflowService;
use Lerp\Product\Table\ProductListTable;
use Lerp\Product\Table\ProductTable;
use Lerp\ProductCalc\Service\ProductCalcService;
use Lerp\ProductCalc\Table\ProductCalcTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ProductCalcServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProductCalcService();
        $service->setLogger($container->get('logger'));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $service->setWorkflowTypes($toolsTable->getEnumValuesPostgreSQL('enum_workflow_type'));
        $service->setProductCalcTable($container->get(ProductCalcTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        $service->setProductListTable($container->get(ProductListTable::class));
        $service->setProductWorkflowService($container->get(ProductWorkflowService::class));
        return $service;
    }
}
