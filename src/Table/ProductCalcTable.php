<?php

namespace Lerp\ProductCalc\Table;

use Lerp\Product\Table\ProductCalcTable as ProductProductCalcTable;
use Lerp\ProductCalc\Entity\ProductCalcEntity;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class ProductCalcTable extends ProductProductCalcTable
{
    protected function makeJoinDefault(Select $select): void
    {
        $select->join('product', 'product.product_uuid = product_calc.product_uuid', ['product_text_part', 'product_text_short'], Select::JOIN_LEFT);
        $select->join('product_no', 'product_no.product_no_uuid = product.product_no_uuid', ['product_no_no'], Select::JOIN_LEFT);
    }

    /**
     * @param string $productCalcUuid
     * @return array
     */
    public function getProductCalc(string $productCalcUuid): array
    {
        $select = $this->sql->select();
        try {
            $this->makeJoinDefault($select);
            $select->where(['product_calc_uuid' => $productCalcUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Get the last productCalc.
     * @param string $productUuid
     * @return array The last productCalc.
     */
    public function getProductCalcLastForProduct(string $productUuid): array
    {
        if (!empty($result = $this->getProductCalcsForProduct($productUuid))) {
            return $result[0];
        }
        return [];
    }

    /**
     * Get all product calc for one product (ORDER BY product_calc_time_create DESC).
     * @param string $productUuid
     * @return array
     */
    public function getProductCalcsForProduct(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $this->makeJoinDefault($select);
            $select->where(['product_calc.product_uuid' => $productUuid]);
            $select->order('product_calc_time_create DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Explicit create a productCalc.
     * @param ProductCalcEntity $productCalcEntity
     * @param string $userUuid
     * @return string
     */
    public function createProductCalcWithEntity(ProductCalcEntity $productCalcEntity, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $productCalcEntity->unsetReadonly();
        $storage = $productCalcEntity->getStorage();
        $storage['product_calc_uuid'] = $this->uuid();
        $storage['user_uuid_create'] = $userUuid;
        try {
            $insert->values($storage);
            if ($this->insertWith($insert) > 0) {
                return $storage['product_calc_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * Get the last productCalc or create one and return it.
     * @param string $productUuid
     * @param string $quantityunitUuid
     * @param string $userUuid
     * @return array
     */
    public function createGetProductCalc(string $productUuid, string $quantityunitUuid, string $userUuid): array
    {
        if (empty($productCalc = $this->getProductCalcLastForProduct($productUuid))) {
            $insert = $this->sql->insert();
            $uuid = $this->uuid();
            try {
                $insert->values([
                    'product_calc_uuid' => $uuid,
                    'product_uuid'      => $productUuid,
                    'quantityunit_uuid' => $quantityunitUuid,
                    'user_uuid_create'  => $userUuid,
                ]);
                if ($this->insertWith($insert) > 0) {
                    $productCalc = $this->getProductCalc($uuid);
                }
            } catch (\Exception $exception) {
                $this->log($exception, __CLASS__, __FUNCTION__);
            }
        }
        return $productCalc;
    }

    /**
     * @param ProductCalcEntity $productCalcEntity
     * @param string $userUuid
     * @return int
     */
    public function updateProductCalcEntity(ProductCalcEntity $productCalcEntity, string $userUuid): int
    {
        $update = $this->sql->update();
        $storage = $productCalcEntity->getStorage();
        $uuid = $storage['product_calc_uuid'];
        unset($storage['product_calc_uuid']);
        if (isset($storage['product_uuid'])) {
            unset($storage['product_uuid']);
        }
        $productCalcEntity->unsetReadonly();
        $storage['product_calc_time_update'] = date('Y-m-d H:i:s');
        $storage['user_uuid_update'] = $userUuid;
        try {
            $update->set($storage);
            $update->where(['product_calc_uuid' => $uuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
