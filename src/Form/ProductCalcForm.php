<?php

namespace Lerp\ProductCalc\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Uuid;

/**
 * Class ProductCalcForm
 * @package Lerp\ProductCalc\Form
 *
 * New ProductCalc does not require a Form. Therefore product_calc_uuid is allways required.
 *
 */
class ProductCalcForm extends AbstractForm implements InputFilterProviderInterface
{
    protected bool $productUuidAvailable = false;

    public function setProductUuidAvailable(bool $productUuidAvailable): void
    {
        $this->productUuidAvailable = $productUuidAvailable;
    }

    public function init()
    {
        $this->add(['name' => 'product_calc_uuid']);
        if ($this->productUuidAvailable) {
            $this->add(['name' => 'product_uuid']);
        }
        $this->add(['name' => 'quantityunit_uuid']);
        $this->add(['name' => 'product_calc_cost_base']);
        $this->add(['name' => 'product_calc_cost_work']);
        $this->add(['name' => 'product_calc_cost_work_extern']);
        $this->add(['name' => 'product_calc_percent_mgk']);
        $this->add(['name' => 'product_calc_percent_fgk']);
        $this->add(['name' => 'product_calc_percent_vvgk']);
        $this->add(['name' => 'product_calc_percent_profit']);
        $this->add(['name' => 'product_calc_percent_profit_extern']);
        $this->add(['name' => 'product_calc_price_set']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['product_calc_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => Uuid::class]
            ]
        ];
        if ($this->productUuidAvailable) {
            $filter['product_uuid'] = [
                'required'   => true,
                'filters'    => [
                    ['name' => FilterChainStringSanitize::class]
                ],
                'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['quantityunit_uuid'] = [
            'required'   => true,
            'filters'    => [
                ['name' => FilterChainStringSanitize::class]
            ],
            'validators' => [
                ['name' => Uuid::class]
            ]
        ];

        $filter['product_calc_cost_base'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_cost_work'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_cost_work_extern'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_percent_mgk'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_percent_fgk'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_percent_vvgk'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_percent_profit'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_percent_profit_extern'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['product_calc_price_set'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class]
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        return $filter;
    }
}
