<?php

namespace Lerp\ProductCalc;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\ControllerProviderInterface;
use Laminas\ModuleManager\Feature\ServiceProviderInterface;
use Laminas\ModuleManager\Feature\ViewHelperProviderInterface;

class Module implements ConfigProviderInterface, ServiceProviderInterface, ControllerProviderInterface, ViewHelperProviderInterface
{
    /**
     * Returns configuration to merge with application configuration
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.
     * @return array|\Laminas\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
            ]
        ];
    }

    /**
     * Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.
     * @return array|\Laminas\ServiceManager\Config
     */
    public function getControllerConfig()
    {
        return [
            'factories' => [
            ]
        ];
    }

    /**
     * Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.
     * @return array|\Laminas\ServiceManager\Config
     */
    public function getViewHelperConfig()
    {
        return [
        ];
    }
}
